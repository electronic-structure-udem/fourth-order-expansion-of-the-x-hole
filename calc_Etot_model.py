from pyscf import gto, dft,scf
from pyscf.data.elements import is_ghost_atom
from pyscf.dft import numint
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import torch
import re
from myRKS import RKS
from myUKS2 import UKS
from jounce_rhox import deriv_jounce_grid,jounce_grid

def deriv_s(wv,rho,aog):
   """
   Compute the reduced gradient contribution to the derivative
   of the model with matrix density
   
   Input:
      wv: Array(ngrids)
            rho*eps_x_pbe*weights*grad_s_NN, where grad_s_NN is the derivative 
            of the model with s.
      rho: Array(6,n_grids): 
            electronic density, gradient of density x,y,z, tau and laplacian
      aog: Array(n_deriv,nao,nao) 
            the ao values and their derivative
      
   return: Array(nao,nao) 
            the reduced gradient contribution to the derivative 
            of the model with the density matrix
   """
   kf = (3*np.pi**2*rho[0])**(1./3.)
   f=np.einsum("ni,n->ni",aog[0],wv)
   nablaf = np.array([aog[1], aog[2], aog[3]])
   nabla_rho = np.array([rho[1],rho[2],rho[3]]) 
   norm=np.sqrt(np.einsum("kn,kn->n",nabla_rho,nabla_rho))
   nablafnablarho=np.einsum("knj,kn->nj",nablaf,nabla_rho)
   s_deriv_1=np.einsum("n,ni,nj->ij",1./(rho[0]*kf*norm),f,nablafnablarho)
   s_deriv_2=2./3.*np.einsum("n,ni,nj",norm/(kf*rho[0]**2),f,aog[0])
   return s_deriv_1-s_deriv_2

def deriv_curv(wv,secondD,rho,aog):
   """
   Compute the  contribution of the reduced curvature of the X hole to 
   the derivative of the model with matrix density
   
   Input:
      wv: Array(ngrids)
            rho*eps_x_pbe*weights*grad_curv_NN, where grad_curv_NN is the derivative 
            of the model with the curvature of the X hole.
      secondD: Array(ngrids)
            Reduced curvature of the X hole
      rho: Array(6,n_grids): 
            electronic density, gradient of density x,y,z, tau and laplacian
      aog: Array(n_deriv,nao,nao) 
            the ao values and their derivative
      
   return: Array(nao,nao) 
            the contribution of the curvature of the X hole to the derivative 
            of the model with the density matrix
   """
   kf = (3*np.pi**2*rho[0])**(1./3.)
   factor_units_SD= rho[0]*kf**2
   f=np.einsum("ni,n->ni",aog[0],wv/factor_units_SD)
   nablaf = np.array([aog[1], aog[2], aog[3]])
   nabla2f = aog[4] + aog[7] + aog[9]
   nabla_rho = np.array([rho[1],rho[2],rho[3]])
   nabla_squared=np.einsum("kn,kn->n",nabla_rho,nabla_rho)
   ff2=2*np.einsum("ni,nj->ij",f,nabla2f)
   nablafnablarho=np.einsum("knj,kn->nj",nablaf,nabla_rho)
   dQ_3_1 = 4*np.einsum("n,ni,nj->ij",1./rho[0],f,nablafnablarho)
   dQ_3_2=np.einsum("ni,nj,n,n->ij",f,aog[0],nabla_squared,1./rho[0]**2)
   SD_deriv=-1./3.*(ff2+0.5*(dQ_3_1-dQ_3_2))
   SD_deriv-=np.einsum("n,ni,nj->ij",secondD*5./3.*kf**2,f,aog[0])# secondD is already in reduced units
   return SD_deriv

def calc_Exc_4D(molecule=None,positions=None,spin=None,model=None,basis="def2-tzvp",
               mol=None,mf=None,dm=None,calc_Ec_PBE=True,noderiv=False):
   """
   Calculate the XC energy of the model which depends on the fourth derivative
   of the X hole
   
   Molecule: string
      name of the molecule
   Positions: list of list (natoms, x,y,z)
      XYZ positions for each atoms
   Spin: int
      spin of the molecule
   Model : pytorch NN
   Mol: pyscf mol object
   mf: pyscf mf object
   dm: Array(nao,nao)
      density matrix
   calc_Ec_PBE: bool to add PBE's correlation to the model
   noDeriv: bool to calculate derivatives or not
   
   Return: PBE Exc energy (float), model Exc energy (float),
           XC contribution to fock matrix 
      
   """
   # to compute the exchange energy for a pyscf mol object if none exist
   if mol is None:
      mol_name=molecule
      mol = gto.Mole()
      atoms = re.findall('[A-Z][^A-Z]*', molecule)
      molecule =[]
      nAtom=0
      for atom in atoms:
         atom_pos = positions[nAtom]
         molecule.append([atom,(atom_pos[0],atom_pos[1],atom_pos[2])])
         nAtom=nAtom+1
      mol.atom=molecule
      mol.spin=spin
      mol.basis = basis # downloaded from BSE
      mol.build()
      mf = dft.KS(mol,xc="pbe,pbe")
      mf.kernel(max_cycle=100)
      dm = mf.make_rdm1()
      
   if model is None: model=mf.modelNN
   unrestricted = int(isinstance(mf,UKS)) or int(isinstance(mf,dft.uks.UKS))
   coords = mf.grids.coords
   weights = mf.grids.weights
   aog = numint.eval_ao(mol, coords, deriv=4)
   #
   fourthDA=np.zeros(weights.shape[0])
   secondDA=np.zeros(weights.shape[0])
   
   fourthDB=np.zeros(weights.shape[0])
   secondDB=np.zeros(weights.shape[0])
   Ex_4der=0
   Ex_pbe=0
   ### RESTRICTED CALCULATION
   if unrestricted==0:
        ### compute ingredients from PYSCF
        rho = numint.eval_rho(mol, aog, dm/2., xctype='MGGA')
        rhoA=rho
        sA=1./(2.*(3.*np.pi**2)**(1./3.))*np.linalg.norm(rho[1:4].T,axis=1)/rho[0]**(4./3.)
        sB=sA
        eps_x_pbeA,temp= dft.libxc.eval_xc("pbe,", [rho,np.zeros(weights.shape[0])],spin=5)[:2]
        eps_x_temp,va_pbeA= dft.libxc.eval_xc("pbe,", rhoA*2,spin=0)[:2]
        rhotot=2*rho
        ### Compute fourth and seconde derivative
        fourthDA,secondDA=jounce_grid(rho,dm/2.,aog) #fast

        #for iG in range(0,weights.shape[0]): #slow
        #   #fourthDA[iG],secondDA[iG]=reduced_deriv(rho[:,iG], mog[:,iG,:])
        #   temp,temp,fourthDA[iG],secondDA[iG]=reduced_jounce_ao_deriv(rho[:,iG], dm/2.,aog[:,iG,:])
        
        ### Compute the factor from the neural network and it's derivative
        XA=np.column_stack((secondDA,fourthDA,sA))
        #XA=sA.reshape(-1,1)
        
        XA=torch.tensor(XA,requires_grad=True)
        factorA=model["regr"](XA)

        gradA,=torch.autograd.grad(factorA,XA,create_graph=True,grad_outputs=torch.ones_like(factorA))
        gradA=gradA.detach().numpy()
        factorA = np.nan_to_num(factorA.detach().numpy()[:,0],1)
        
        ### Compute derivative of the model with density matrix
        Fmunu=0.
        wvA=rhoA[0]*eps_x_pbeA*weights*gradA.T
        #for iG in range(0,weights.shape[0]):
        #   gfourthD,gsecondD,temp1,temp2=reduced_jounce_ao_deriv(rho[:,iG], dm/2.,aog[:,iG,:])
        #   #gS,grho=g_s(rho[:,iG],aog[:,iG,:])
        #   Fmunu+=wvA[2,iG]*gS+wvA[1,iG]*gfourthD+wvA[0,iG]*gsecondD
        if noderiv==False:
          Fmunu_pbe_factor=numint.eval_mat(mol,aog,weights,2*rhoA,(va_pbeA[0]*factorA,va_pbeA[1]*factorA,None,None) ,xctype='gga',spin=0)
          Fmunu+=Fmunu_pbe_factor
          Fmunu+=deriv_s(wvA[2],rhoA,aog)
          Fmunu+=deriv_curv(wvA[0],secondDA,rhoA,aog)
          Fmunu+=deriv_jounce_grid(wvA[1],fourthDA,rhoA,aog,dm/2.)
        Ex_4der=np.sum(weights*rho[0]*eps_x_pbeA*2*factorA)
        Ex_pbe=np.sum(weights*rho[0]*eps_x_pbeA*2)
   ### UNRESTRICTED CALCULATION
   else: 
        ### Compute ingredients with PYSCF
        if dm is None:dm = mf.make_rdm1()
        dmA=dm[0]
        dmB=dm[1]
        rhoA = numint.eval_rho(mol, aog, dmA, xctype='MGGA')
        rhoB = numint.eval_rho(mol, aog, dmB, xctype='MGGA')
        sA=1./(2.*(3.*np.pi**2)**(1./3.))*np.linalg.norm(rhoA[1:4].T,axis=1)/rhoA[0]**(4./3.)
        sB=1./(2.*(3.*np.pi**2)**(1./3.))*np.linalg.norm(rhoB[1:4].T,axis=1)/rhoB[0]**(4./3.)
        eps_x_pbeA,vA_pbe,= dft.libxc.eval_xc("pbe,", [rhoA,np.zeros(weights.shape[0])],spin=5)[:2] 
        eps_x_pbeB,vB_pbe,= dft.libxc.eval_xc("pbe,", [rhoB,np.zeros(weights.shape[0])],spin=5)[:2] 
        rhotot=rhoA+rhoB
        #
        Ex_pbe=np.sum(weights*rhoA[0]*eps_x_pbeA)
        
        ### COmpute fourth and second derivative of rhoX
        fourthDA,secondDA=jounce_grid(rhoA,dmA,aog) #fast
        #for iG in range(0,weights.shape[0]): #slow
           #fourthDA[iG],secondDA[iG]=reduced_deriv(rhoA[:,iG], mogA[:,iG,:])
         #  temp,temp,fourthDA[iG],secondDA[iG]=reduced_jounce_ao_deriv(rhoA[:,iG], dmA,aog[:,iG,:])
        if rhoB[0][0]>1e-30:
           fourthDB,secondDB=jounce_grid(rhoB,dmB,aog) #fast 
              #fourthDB[iG],secondDB[iG]=reduced_deriv(rhoB[:,iG], mogB[:,iG,:])
              #temp,temp,fourthDB[iG],secondDB[iG]=reduced_jounce_ao_deriv(rhoB[:,iG], dmB,aog[:,iG,:])
        
        ### Compute factor and derivative ffrom NN
        XA=np.column_stack((secondDA,fourthDA,sA))
        #XA=sA.reshape(-1,1)
        XA=torch.tensor(XA,requires_grad=True)
        factorA=model["regr"](XA)
        gradA,=torch.autograd.grad(factorA,XA,create_graph=True,grad_outputs=torch.ones_like(factorA))
        factorA = np.nan_to_num(factorA.detach().numpy()[:,0],1)
        gradA=gradA.detach().numpy()
        Ex_4der=np.sum(weights*rhoA[0]*eps_x_pbeA*factorA) 
        
        ###Compute derivative of model with density matrix
        FmunuA,FmunuB=np.zeros(dm.shape)     
        eps_x_temp,va_pbeA= dft.libxc.eval_xc("pbe,", rhoA*2,spin=0)[:2]
        wvA=rhoA[0]*eps_x_pbeA*weights*gradA.T
        #for iG in range(0,weights.shape[0]):
        #   gfourthD,gsecondD,temp1,temp2=reduced_jounce_ao_deriv(rhoA[:,iG], dmA,aog[:,iG,:])
        #   #gS,grho=g_s(rhoA[:,iG],aog[:,iG,:])
        #   FmunuA+=wvA[2,iG]*gS+wvA[1,iG]*gfourthD+wvA[0,iG]*gsecondD
        if noderiv==False:
         Fmunu_pbe_factorA=numint.eval_mat(mol,aog,weights,2*rhoA,(va_pbeA[0]*factorA,va_pbeA[1]*factorA,None,None) ,xctype='gga',spin=0)
         FmunuA+=Fmunu_pbe_factorA
         FmunuA+=deriv_s(wvA[2],rhoA,aog)
         FmunuA+=deriv_curv(wvA[0],secondDA,rhoA,aog)
         FmunuA+=deriv_jounce_grid(wvA[1],fourthDA,rhoA,aog,dmA)
        #
        ### Down spin part
        if rhoB[0][0]>1e-30:
          Ex_pbe+=np.sum(weights*rhoB[0]*eps_x_pbeB)
          XB=np.column_stack((secondDB,fourthDB,sB))
          #XB=sB.reshape(-1,1)
          XB=torch.tensor(XB,requires_grad=True)
          factorB=model["regr"](XB)
          gradB,=torch.autograd.grad(factorB,XB,create_graph=True,grad_outputs=torch.ones_like(factorB))
          gradB=gradB.detach().numpy()
          factorB = np.nan_to_num(factorB.detach().numpy()[:,0],1)
          Ex_4der+=np.sum(weights*rhoB[0]*eps_x_pbeB*factorB)
          eps_x_temp,vb_pbeB= dft.libxc.eval_xc("pbe,", rhoB*2,spin=0)[:2]
          
          wvB=rhoB[0]*eps_x_pbeB*weights*gradB.T
          #for iG in range(0,weights.shape[0]):
          #  gfourthD,gsecondD,temp1,temp2=reduced_jounce_ao_deriv(rhoB[:,iG], dmB,aog[:,iG,:])
          #  #gS,grho=g_s(rhoB[:,iG],aog[:,iG,:])
          #  FmunuB+=wvB[1,iG]*gfourthD+wvB[0,iG]*gsecondD
          if noderiv==False:
            Fmunu_pbe_factorB=numint.eval_mat(mol,aog,weights,2*rhoB,(vb_pbeB[0]*factorB,vb_pbeB[1]*factorB,None,None) ,xctype='gga',spin=0)
            FmunuB+=Fmunu_pbe_factorB
            FmunuB+=deriv_s(wvB[2],rhoB,aog)
            FmunuB+=deriv_curv(wvB[0],secondDB,rhoB,aog)
            FmunuB+=deriv_jounce_grid(wvB[1],fourthDB,rhoB,aog,dmB)
   print('Ex pbe',Ex_pbe)
   print ('Ex 4der',Ex_4der)
   if unrestricted>0:### compute correlation contribution of PBE spin unrestricted
      if calc_Ec_PBE==True:
         eps_c_pbe,vc_pbe,= dft.libxc.eval_xc(",pbe", [rhoA,rhoB],spin=5)[:2]
         Ec_pbe = np.sum(rhotot[0]*weights*eps_c_pbe)
         vrho,vsigma=vc_pbe[:2]
         vkc_pbeA = numint.eval_mat(mol,aog,weights,(rhoA,rhoB),[vrho[:,0],[vsigma[:,0],vsigma[:,1]]], xctype='GGA',spin=5) 
         vkc_pbeB = numint.eval_mat(mol,aog,weights,(rhoB,rhoA),[vrho[:,1],[vsigma[:,2],vsigma[:,1]]], xctype='GGA',spin=5) 
         return Ex_pbe+Ec_pbe,Ex_4der+Ec_pbe,FmunuA+vkc_pbeA,FmunuB+vkc_pbeB
      else:
         return Ex_pbe,Ex_4der,FmunuA,FmunuB
      
   else:### compute correlation contribution of PBE spin restricted
      if calc_Ec_PBE==True:
         eps_c_pbe,vc_pbe,= dft.libxc.eval_xc(",pbe", rhotot,spin=0)[:2]
         Ec_pbe = np.sum(rhotot[0]*weights*eps_c_pbe)
         vkc_pbe = numint.eval_mat(mol,aog,weights,rhotot,vc_pbe, xctype='gga',spin=0) 
         #print("test",Ec_pbe,vkc_pbe)
         return Ex_pbe+Ec_pbe,Ex_4der+Ec_pbe,Fmunu+vkc_pbe
      else:
         return Ex_pbe,Ex_4der,Fmunu

def get_vxc(mol,dm,mf, *args, **kwargs):
    """
    Custom get_vxc fonction for the model
    Input:
      mol: pyscf mol object
      dm: array(nao,nao)
          density matrix
      mf: pyscf mf object
      
    return:
    vk: Array(nao,nao)
        XC contribution to potential
    Ex_4der: float
             XC energy of model

    """
    Exc_pbe,Exc_4der,vxc=calc_Exc_4D(mol=mol,mf=mf,dm=dm)
    return (vxc+vxc.T)/2., Exc_4der
 
def Uget_vxc(mol,dm,mf, *args, **kwargs):
    """
    Custom get_vxc function for the model
    Input:
      mol: pyscf mol object
      dm: array(nao,nao)
          density matrix
      mf: pyscf mf object
      vk: Array(nao,nao)
        XC contribution to potential
      Ex_4der: float
             XC energy of model
     """
    Ex_pbe,Ex_4der,vxcA,vxcB=calc_Exc_4D(mol=mol,mf=mf,dm=dm)

    return np.array([(vxcA+vxcA.T)/2.,(vxcB+vxcB.T)/2.]), Ex_4der
    
def calc_Etot_SCF(molecule,positions,spin,model,basis="def2-tzvp",max_cycle=100):
   """
   Compute the total SCF energy for the model
   Input:
      Molecule: string
         string of the molecule name
      positions: list of list
         X,Y,Z positions of each atoms in the molecule
      Spin: int
         Spin of the molecule
      model: pytorch NN object
         the model
      basis: string
         basis set for pyscf
      max_cycle: int
         number of scf cycle. If <=0, the calculation is post-pbe
   return:
      Etot_pbe: float
      Etot_model: float
   """
   mol = gto.Mole()
   mol.mol_name=molecule
   atoms = re.findall('[A-Z][^A-Z]*', molecule)
   molecule =[]
   nAtom=0
   for atom in atoms:
      atom_pos = positions[nAtom]
      molecule.append([atom,(atom_pos[0],atom_pos[1],atom_pos[2])])
      nAtom=nAtom+1
   mol.atom=molecule
   mol.spin=spin
   mol.basis = basis # downloaded from BSE
   mol.verbose=4
   mol.build()
   mf1 = dft.KS(mol=mol,xc="pbe,pbe")
   #mf1=scf.HF(mol=mol)
   mf1.grids.level=4

   mf1.kernel()
   if mol.spin>0:
      mf = UKS(mol)
      mf.CF_get_vxc = Uget_vxc
   else:
      mf=RKS(mol)
      mf.CF_get_vxc=get_vxc
   mf.modelNN=model
   mf.xc="HF,"  
   mf.max_cycle=max_cycle
   mf.grids.level=4
   mf.conv_tol=1e-8
   mf.grids.radi_method=dft.radi.delley

   mf.kernel(dm0=mf1.make_rdm1())# uses pbe orbital as initial guess
   return mf1.e_tot,mf.e_tot
   
if __name__ == '__main__':
   from torch_opt_NN import NN
   import pickle
   model={}
   model["regr"]=NN()
   model["regr"]=model["regr"].load_from_checkpoint(checkpoint_path="NNx4.ckpt")
   #calc_Exc_4D(molecule="H",positions=[[0,0,0]],spin=1,model=model,basis="def2-tzvp")
   calc_Etot_SCF("Ar",[[0, 0, 0]],0,model,basis="def2-tzvp")


