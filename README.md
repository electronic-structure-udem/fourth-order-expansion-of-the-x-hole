# Fourth-order expansion of the X hole


## Approximations to the XC energy using the fourth-order term of the expansion of the X hole
This repository contains the Python code accompagning the article: The fourth-order expansion of the exchange hole and neural networks to construct exchange-correlation functionals

## Description
jounce_rhox.py: To calculate the fourth order term of the X hole using PySCF

torch_opt_NN.py: To train a NN to reproduce the atomization energy of PBE0 using the gradient of the density, the second and fourth order term of the X hole. Four molecule are considered: H2O, P2, C2H4 and N2.

calc_Etot_model.py: To calculate the total electronic energy using the NN trained from torch_opt_NN.py. By default, the calculation are done self-consistently

myRKS.py and myUKS2.py: Modified PySCF functions so the models can be calculated self-consistently

Various .pt files: Training data for the NN

NNx4.ckpt and NNHx4.ckpt: Trained parameters for the NN


## Example Usage
To calculate the energy of the Ar atom with NNx4: python calc_Etot_model.py

To train the parameters of NNx4: python torch_opt_NN.py

To change the model to NNHx4, see the comments in the torch_opt_NN.py file.



