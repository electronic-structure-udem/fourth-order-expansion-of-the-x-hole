from re import T
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.nn import functional as F
from torch import nn
from pytorch_lightning.core.lightning import LightningModule
from torch.optim import Adam
from pytorch_lightning import Trainer, seed_everything
from torch.utils.data import TensorDataset, DataLoader#load data
from torch.utils.data import Dataset
from pytorch_lightning.loggers import TensorBoardLogger
import h5py

#
class AtomiDataset(Dataset):
    def __init__(self):
        self.molecules=["NN","OHH","CCHHHH","PP"]
        self.atoms=["N","O","H","C","P"]
        self.systems=["N","NN","OHH","O","H","CCHHHH","C","PP","P"]
        
        #load data
        self.data=torch.load(self.systems[0]+".pt")
        self.pos_init={self.systems[0]:0}
        self.pos_end={self.systems[0]:len(self.data)}
        for i in range(len(self.systems)):
            if i==0:continue
            data_mol=torch.load(self.systems[i]+".pt")
            self.data=torch.cat((self.data,data_mol),0)
            self.pos_init[self.systems[i]]=self.pos_end[self.systems[i-1]]
            self.pos_end[self.systems[i]]=self.pos_init[self.systems[i]]+len(data_mol)      
        
        #to save the parameters used for standardization
        temp=self.data.clone()
        temp[:,0]=torch.where(self.data[:,0]>=0,torch.log(1+self.data[:,0]),-torch.log(1-self.data[:,0]))
        temp[:,1]=torch.where(self.data[:,1]>=0,torch.log(1+self.data[:,1]),-torch.log(1-self.data[:,1]))
        temp[:,2]=torch.log(1.+self.data[:,2])
        means = temp.mean(dim=0, keepdim=True)
        stds = temp.std(dim=0, keepdim=True)
        del temp
        torch.save(means,"means.pt")
        torch.save(stds,"stds.pt")

        # calculated pbe0 results with def2-tzvp
        self.conv_ua_kcalmol=627.5
        self.atomi_Ex_pbe0={"NN":15.21,"PP":-14.2,"NO":8.0,"OHH":92.58,"CH4":179.5,
                            "CCHHHH":255.6,"H2":23.93,"O2":11.3,"F2":-15.6}
        self.Ex_pbe0={"N":-6.547132049989617,"NN":-13.11850557356761,"O2":-16.3351416821599,
                      "P":-22.5213754329174,"PP":-45.0201636856424,"NO":-14.718450832993963,
                      "H":-0.30416879043966233,"O":-8.158533117169764,"OHH":-8.914408048632815,
                      "CH4":-6.5326568628375625,"CCHHHH":-11.6837082516633,"C":-5.029872068381621,
                      "H2":-0.6464762249084878,"F2":-19.9414954331967,"F":-9.98320046329374}

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]

class NN(LightningModule):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Linear(3, 12).double()
        self.layer_2 = nn.Linear(12, 12).double()
        self.layer_3 = nn.Linear(12, 1).double()
        self.data=None
        try:
            self.std_means=torch.load("means.pt")[:,:-1]
            self.std_stds=torch.load("stds.pt")[:,:-1]
        except OSError as e:
            dataset=AtomiDataset()
            self.std_means=torch.load("means.pt")[:,:-1]
            self.std_stds=torch.load("stds.pt")[:,:-1]
        
    def train_dataloader(self):
        dataset=AtomiDataset()
        self.data=DataLoader(dataset,batch_size=dataset.__len__(),shuffle=False)
        return self.data


    def forward(self, x):
        t=torch.empty((x.shape[0],3),dtype=torch.float64)
        t[:,0]=torch.where(x[:,0]>=0,torch.log(1+x[:,0]),-torch.log(1-x[:,0]))
        t[:,1]=torch.where(x[:,1]>=0,torch.log(1+x[:,1]),-torch.log(1-x[:,1]))
        t[:,2]=torch.log(1.+x[:,2])
        t= (t-self.std_means)/self.std_stds
        t = self.layer_1(t)
        t = torch.nn.functional.gelu(t)
        t = self.layer_2(t)
        t = torch.nn.functional.gelu(t)
        t = self.layer_3(t)
        return t
        ############### FOR NNHX4###########
        #temp= (torch.tanh(x[:,2].reshape(-1,1))**2+torch.tanh(x[:,0].reshape(-1,1)-0.63496042078)**2+\
        #    torch.tanh(x[:,1].reshape(-1,1)+1.0367350)**2)*t
        #return 2./(1.+torch.exp(-temp))

    
    def atomi_loss(self,pred,y):
        conv=self.data.dataset.conv_ua_kcalmol
        #
        pos_init=self.data.dataset.pos_init
        pos_end=self.data.dataset.pos_end
        
        # atomization and total energy dataset of pbe0
        dataset_atomi_Ex_pbe0=self.data.dataset.atomi_Ex_pbe0
        dataset_Ex_pbe0=self.data.dataset.Ex_pbe0
        
        # calculate prediction
        pred=pred*y
        Ex_calc={}
        #calcule exchange energies
        for mol in self.data.dataset.systems:
            Ex_calc[mol]=torch.sum(pred[pos_init[mol]:pos_end[mol]])
        
        #calculate atomisation energies 
        atomi_calc={}   
        for molecule in self.data.dataset.molecules:
            E_atoms=0
            for atom in molecule:
                E_atoms+=Ex_calc[atom]
            atomi_calc[molecule]=(E_atoms-Ex_calc[molecule])*conv
            
        #calculate total errors
        error_Etot=0.  
        for mol in self.data.dataset.systems:
            error_Etot+=(dataset_Ex_pbe0[mol]-Ex_calc[mol])**2
        
        #atomization errors
        error_atomi=0.  
        for mol in self.data.dataset.molecules:
            error_atomi+=(atomi_calc[mol]-dataset_atomi_Ex_pbe0[mol])**2
        
        #            
        self.log("error_Etot", error_Etot, on_step=False, on_epoch=True, prog_bar=True, logger=True)
        self.log("error_atomi", error_atomi, on_step=False, on_epoch=True, prog_bar=True, logger=True)
        total_error=error_atomi/1000+error_Etot
        self.log("total_error", total_error, on_step=False, on_epoch=True, prog_bar=False, logger=True)
        return total_error
        
    def training_step(self, batch, batch_idx):
        x =batch[:,:-1]
        y= batch[:,-1].reshape(-1,1)
        pred = self.forward(x)
        train_loss = self.atomi_loss(pred,y)
        return train_loss

    def configure_optimizers(self):
        optimizer = torch.optim.LBFGS(self.parameters(),lr=1e-3)
        return {"optimizer":optimizer}

if __name__ == '__main__':
    seed_everything(22, workers=True)

    model = NN()
    #model = model.load_from_checkpoint(checkpoint_path="NNx4.ckpt")
    #print(model)
    logger=TensorBoardLogger("tb_logs",name="model_test")
    trainer = Trainer(precision=64,logger=logger,max_epochs=1000,checkpoint_callback=True,
                      deterministic=True,auto_lr_find=False)
    #trainer.tune(model)
    trainer.fit(model)
    trainer.save_checkpoint("test.ckpt")
