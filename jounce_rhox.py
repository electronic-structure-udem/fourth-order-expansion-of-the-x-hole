import numpy as np

#eval_ao derivatives order 
#        1st derivatives (3,N,nao) for x, y, z;
#        Then 2nd derivatives (6,N,nao) for xx, xy, xz, yy, yz, zz;
#        Then 3rd derivatives (10,N,nao) for xxx, xxy, xxz, xyy, xyz, xzz, yyy, yyz, yzz, zzz;
#        Then 4th derivatives (,N,nao) for xxxx, xxxy, xxxz, xxyy , xxyz, xxzz, xyyy, xyyz, xyzz, xzzz, yyyy, yyyz, yyzz, yzzz, zzzz

d = {'f':0, 'x':1, 'y':2, 'z':3, 'xx':4, 'xy': 5, 'xz':6, 'yy':7, 'yz':8, 'zz':9, 'xxx':10, 'xxy':11, 'xxz':12, 'xyy':13, 'xyz':14, 'xzz':15, 'yyy':16, 'yyz':17, 'yzz':18, 'zzz':19 }

def jounce_mo(rho, mog): 
    """
    compute the 4th derivative and 2nd of the X hole for a grid point
    in the MO bassis
    
    Input:
        rho: Array(6)
             The electronic density, X,Y,Z gradient, laplacian and tau
        mog: Array
            MO orbitals values
    Return:
        FD: float
            4th derivative of rho X
        SD: float
            2nd derivative of rho X
    """
    
    #print(rho)
    rhog = rho[0]
    nabla_rho = rho[1], rho[2], rho[3]
    lap = rho[4]
    tau = rho[5]

    f = mog[0]
    nablaf = mog[1], mog[2], mog[3]
    nabla2f = mog[4] + mog[7] + mog[9]
    nabla3f = mog[10] + mog[13] + mog[15], mog[11] + mog[16] + mog[18], mog[12]+ mog[17] + mog[19] 
    nabla4f = mog[20] + mog[30] + mog[34] + 2.*mog[23] + 2.*mog[32] + 2.*mog[25]
    f6v = mog[4], mog[7], mog[9], np.sqrt(2.)*mog[5], np.sqrt(2.)*mog[6],np.sqrt(2.)*mog[8]
    # Compute term A
    A = 2.*rhog*np.dot(f,nabla4f)
    #print(A)
    # compute term B
    B = np.einsum('j,ij-> i', f, nabla3f)
    B = np.dot(nabla_rho, B)
    B *= 4.
    #print(B)
    # compute term C
    C = 0.5*(lap-4.*tau)**2
    #print(C)
    #compute term D
    D =  np.einsum('j,ij-> i', f, f6v)
    D = np.dot(D,D)
    D *= 4.
    #print(D)
    grad_squared = rho[1]**2 + rho[2]**2 + rho[3]**2
    #SD = -1./6. *  (lap-2.*tau + 0.5*np.dot(nabla_rho,nabla_rho)/rhog)
    SD = -1./3. *  (lap-4.*tau + 0.5*grad_squared/rhog)
    FD=-1./(5*rhog) * (A+B+C+D)
    return FD, SD
# Test with Be
def jounce_ao_deriv(rho,dm,aog):
   """
    compute the 4th derivative and 2nd of the X hole for a grid point
    in the AO basis and their derivative with density matrix
    
    Input:
        rho: Array(6)
             The electronic density, X,Y,Z gradient, laplacian and tau
        dm: Array(nao,nao)
            density matrix
        aog:Array (nao)
            ao orbital values
    Return:
        FD: float
            4th derivative of rhoX
        deriv: Array(nao,nao)
               derivative of FD with density matrix
        SD: float
            2nd derivative of rhoX
        SD_deriv: Array(nao,nao)
            derivative of SD with density matrix
   """
   rhog=rho[0]
   f = aog[0]
   nablaf = np.array([aog[1], aog[2], aog[3]])
   nabla_rho = np.array([rho[1],rho[2],rho[3]])
   tau=rho[5]*2 #be careful with the different definitions of tau
   nabla2f = aog[4] + aog[7] + aog[9]
   lap = rho[4]
   nabla3f = np.array([aog[10] + aog[13] + aog[15], aog[11] + aog[16] + aog[18], aog[12]+ aog[17] + aog[19]] )
   nabla4f = aog[20] + aog[30] + aog[34] + 2.*aog[23] + 2.*aog[32] + 2.*aog[25]
   f6v = np.array([aog[4], aog[7], aog[9], np.sqrt(2.)*aog[5], np.sqrt(2.)*aog[6],np.sqrt(2.)*aog[8]])
   #for A and dA
   ff=np.einsum("i,j->ij",f,f) 
   ff4=np.einsum("i,j->ij",f,nabla4f) 
   dmff4=np.einsum("ij,ij->",dm,ff4)
   A=2*rhog*dmff4
   dA= 2*(ff*dmff4+rhog*ff4)
   #for B and dB   
   ff3 = np.einsum("i,kj->ikj",f,nabla3f)
   dmff3=np.einsum("ij,ikj->k",dm,ff3)
   ff1=2*(np.einsum("i,kj->ikj",f,nablaf))
   dB=4*(np.dot(nabla_rho,ff3)+np.dot(dmff3,ff1))
   B=4*np.dot(nabla_rho,dmff3)
#
   #for C and dC
   C = 0.5*(lap-2.*tau)**2
   ff2=2*np.einsum("i,j->ij",f,nabla2f)
   dC=(lap-2*tau)*(ff2)
#
   #for D and dD
   ff6=np.einsum("i,kj->ikj",f,f6v)
   dmff6=np.einsum("ij,ikj->k",dm,ff6)
   D=np.dot(dmff6,dmff6)
   D*=4
   dD=8*(np.dot(dmff6,ff6))
   #
#
   deriv=-1./(5*rhog) * (dA+dB+dC+dD)+1/(5*rhog**2)*(A+B+C+D)*ff
   #second derivative
   SD=-1./3.*(lap-2*tau+0.5*np.dot(nabla_rho,nabla_rho)/rhog)
   dtau = np.einsum("ki,kj->ij",nablaf,nablaf)
   dlap = ff2+2*dtau
   nabla_squared=np.dot(nabla_rho,nabla_rho)
   dQ_3=0.5*(rhog*2*np.dot(nabla_rho,ff1)-ff*nabla_squared)/rhog**2
   SD_deriv=-1./3.*(dlap-2*dtau+dQ_3)
   #
   return -1./(5*rhog) * (A+B+C+D),deriv,SD,SD_deriv

def g_s(rho,aog):
   """
   compute the derivative of the reduced gradiant with density matrix for a grid point
   
    Input:
        rho: Array(6)
             The electronic density, X,Y,Z gradient, laplacian and tau
        aog:Array (nao)
            ao orbital values
    return:
        g_s: Array(nao,nao)
        The derivative of s with density matrix   
   """
   f=aog[0]
   rhog=rho[0]
   kf=(3*np.pi**2*rhog)**(1./3.)
   nabla_rho = np.array([rho[1],rho[2],rho[3]])
   ff=np.einsum("i,j->ij",f,f)
   nablaf = np.array([aog[1], aog[2], aog[3]])
   ff1= 2*np.einsum("i,kj->ikj",f,nablaf)
   norm = np.sqrt(np.dot(nabla_rho,nabla_rho))
   d_s=(kf*rhog*np.dot(nabla_rho,ff1)/norm-norm*kf*ff*4./3.)/(2*kf**2*rhog**2)
   return d_s
   
def reduced_jounce_ao_deriv(rho,dm,aog):
    """
    compute the reduced 4th derivative and 2nd of the X hole for a grid point
    in the AO basis and their derivative with density matrix
    
    Input:
        rho: Array(6)
             The electronic density, X,Y,Z gradient, laplacian and tau
        dm: Array(nao,nao)
            density matrix
        aog:Array (nao)
            ao orbital values
    Return:
        reduced deriv: Array(nao,nao)
            derivative of FD with density matrix
        reduced SD_deriv: Array(nao,nao)
            derivative of SD with density matrix
        reduced FD: float
            4th derivative of rhoX
        reduced SD: float
            2nd derivative of rhoX
    """
    FD,gFD, SD,gSD = jounce_ao_deriv(rho, dm,aog)
    kf = (3*np.pi**2*rho[0])**(1./3.)
    factor_units_FD= rho[0]*kf**4
    factor_units_SD= rho[0]*kf**2
    fourthD=FD/factor_units_FD
    secondD=SD/factor_units_SD
    #reduced unit for derivative with dm
    ff=np.einsum("i,j->ij",aog[0],aog[0])
    gsecondD=(factor_units_SD*gSD-SD*5./3.*kf**2*ff)/factor_units_SD**2
    gfourthD=(factor_units_FD*gFD-FD*7./3.*kf**4*ff)/factor_units_FD**2
    return gfourthD,gsecondD,fourthD,secondD

def reduced_deriv(rho,mog):
    """
    compute the reduced 4th derivative and 2nd of the X hole for a grid point
    in the MO bassis
    
    Input:
        rho: Array(6)
             The electronic density, X,Y,Z gradient, laplacian and tau
        mog: Array
            MO orbitals values
    Return:
        reduced FD: float
            4th derivative of rho X
        reduced SD: float
            2nd derivative of rho X
    """
    FD,SD = jounce(rho, mog)
    kf = (3*np.pi**2*rho[0])**(1./3.)
    factor_units_FD= rho[0]*kf**4
    factor_units_SD= rho[0]*kf**2
    fourthD=FD/factor_units_FD
    secondD=SD/factor_units_SD
    return fourthD,secondD
def jounce_grid(rho,dm,aog):
   """
    compute the reduced 4th derivative and 2nd of the X hole for a grid
    in the AO basis
    
    This code is much faster than doing using a "for loop" with the 
    jounce_ao_deriv function, but it's much more complicated 
    
    Input:
        rho: Array(6,ngrid)
             The electronic density, X,Y,Z gradient, laplacian and tau
        dm: Array(nao,nao)
            density matrix
        aog:Array (ngrid,nao)
            ao orbital values
    Return:
        reduced FD: Array(ngrid)
            4th derivative of rhoX
        reduced SD: Array(ngrid)
            2nd derivative of rhoX
   """
   f=aog[0]
   tau=rho[5]*2
   lap = rho[4]
   nabla_rho = np.array([rho[1],rho[2],rho[3]])
   nabla3f = np.array([aog[10] + aog[13] + aog[15], aog[11] + aog[16] + aog[18], aog[12]+ aog[17] + aog[19]] )
   nabla4f = aog[20] + aog[30] + aog[34] + 2.*aog[23] + 2.*aog[32] + 2.*aog[25]
   f6v = np.array([aog[4], aog[7], aog[9], np.sqrt(2.)*aog[5], np.sqrt(2.)*aog[6],np.sqrt(2.)*aog[8]])
   #
   A=2*rho[0]*np.einsum("ni,nj,ij->n",f,nabla4f,dm) 
   #
   dmff3=np.einsum("ij,ni,knj->kn",dm,f,nabla3f)
   B=4*np.einsum("kn,kn->n",nabla_rho,dmff3)
   #
   C=0.5*(lap-2.*tau)**2
   #
   dmff6=np.einsum("ij,ni,knj->kn",dm,f,f6v)
   D=4*np.einsum("kn,kn->n",dmff6,dmff6)
   #
   FD=-1./(5*rho[0]) * (A+B+C+D)
   SD=-1./3.*(lap-2*tau+0.5*np.einsum("kn,kn->n",nabla_rho,nabla_rho)/rho[0])
   #reduced unit
   kf = (3*np.pi**2*rho[0])**(1./3.)
   factor_units_FD= rho[0]*kf**4
   factor_units_SD= rho[0]*kf**2
   fourthD=FD/factor_units_FD
   secondD=SD/factor_units_SD   
   return fourthD,secondD
def deriv_jounce_grid(wv,fourthD,rho,aog,dm):
   """
    compute the reduced derivative of the 4th derivative of the X hole
    with the density matrix
    
    This code is much faster than doing using a "for loop" with the 
    jounce_ao_deriv function, but it's much more complicated 
    
    Input:
      wv: Array(ngrids)
            rho*eps_x_pbe*weights*grad_jounce_NN, where grad_jounce_NN is the derivative 
            of the model with the jounce of the X hole.
        rho: Array(6,ngrid)
             The electronic density, X,Y,Z gradient, laplacian and tau
        dm: Array(nao,nao)
            density matrix
        aog:Array (ngrid,nao)
            ao orbital values
    Return:
        reduced deriv: Array(nao,nao)
            derivative with the density matrix of jounce
   """
   kf = (3*np.pi**2*rho[0])**(1./3.)
   factor_units_FD= rho[0]*kf**4
   f=np.einsum("ni,n->ni",aog[0],wv/factor_units_FD)
   nablaf = np.array([aog[1], aog[2], aog[3]])
   nabla_rho = np.array([rho[1],rho[2],rho[3]])
   tau=rho[5]*2
   nabla2f = aog[4] + aog[7] + aog[9]
   lap = rho[4]
   nabla3f = np.array([aog[10] + aog[13] + aog[15], aog[11] + aog[16] + aog[18], aog[12]+ aog[17] + aog[19]] )
   nabla4f = aog[20] + aog[30] + aog[34] + 2.*aog[23] + 2.*aog[32] + 2.*aog[25]
   f6v = np.array([aog[4], aog[7], aog[9], np.sqrt(2.)*aog[5], np.sqrt(2.)*aog[6],np.sqrt(2.)*aog[8]])   
   
   #dA
   dmff4=np.einsum("ni,nj,ij->n",f,nabla4f,dm)
   Aff=2*np.einsum("n,ni,nj->ij",dmff4/rho[0],aog[0],aog[0])
   dA2=np.einsum("ni,nj->ij",f,nabla4f)
   dA=Aff+2*dA2
   #dB
   dmff3= np.einsum("ij,ni,knj->kn",dm,f,nabla3f)
   dmff3nablaf=np.einsum("kn,knj->nj",dmff3,nablaf)
   nablarhonabla3f=np.einsum("kn,knj->nj",nabla_rho,nabla3f)
   dB1=np.einsum("n,nj,ni->ij",1/rho[0],nablarhonabla3f,f)
   dB2=2*np.einsum("n,nj,ni->ij",1/rho[0],dmff3nablaf,aog[0])
   dB=4*(dB1+dB2)
   dmff3nablarho=np.einsum("kn,kn->n",dmff3,nabla_rho)
   Bff=4*np.einsum("n,n,ni,nj->ij",1./rho[0]**2,dmff3nablarho,aog[0],aog[0])
   #dC
   dC=2*np.einsum("n,ni,nj->ij",(lap-2*tau)/rho[0],f,nabla2f)
   Cff=np.einsum("n,ni,nj->ij",0.5*((lap-2.*tau)/rho[0])**2,f,aog[0])
   #dD
   dmff6 = np.einsum("ij,ni,knj->kn",dm,f,f6v)
   dmff62=np.einsum("kn,knj->nj",dmff6,f6v)
   dD=8*np.einsum("n,nj,ni->ij",1/rho[0],dmff62,aog[0])
   Dff=4*np.einsum("n,nj,ij,ni->n",1/rho[0]**2,dmff62,dm,aog[0])
   Dff=np.einsum("n,ni,nj->ij",Dff,aog[0],aog[0])
   #derivative
   deriv=-1./5. * (dA+dB+dC+dD)+0.2*(Aff+Bff+Cff+Dff)
   deriv-=np.einsum("n,ni,nj",fourthD*7./3.*kf**4,f,aog[0])
   return deriv